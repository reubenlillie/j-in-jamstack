# Leveraging the ‘J’ in JAMstack

A slideshow presentation about the [JAMstack](https://jamstack.org/), built with [Eleventy](https://11ty.io/) and hosted on [Netlify](https://netlify.com/).

First presented for the [Chicago JavaScript MeetUp Group](https://www.meetup.com/js-chi/events/258154877/), 300 S Riverside Plaza #1100, Chicago, IL, USA, August 20, 2019.

[View the slideshow](https://j-in-jamstack.netlify.com/).

[Image Credits](https://j-in-jamstack.netlify.com/credits/)

[References](https://j-in-jamstack.netlify.com/references/)

&copy; 2019 by [Reuben L. Lillie](https://twitter.com/reubenlillie/)

Content is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

Sourcecode is licensed under the [MIT License](https://gitlab.com/reubenlillie/j-in-jamstack/blob/master/LICENSE).
