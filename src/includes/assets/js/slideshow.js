/**
 * @file Make the page into a slideshow presentation
 * @author Reuben L. Lillie
 */

(function () {

  'use strict'

  // Variables
  var form = document.getElementById('goToSlideNumber')
  var slideNumber = document.getElementById('slideNumber')
  var slideNodes = document.querySelectorAll('#slides > section')
  var slides = Array.prototype.slice.call(slideNodes)
  var start = document.getElementById('start')
  var currentSlide = 0

  /**
   * Display a particular slide
   * @param {Number} n The array index of the slide to display
   */
  var goToSlide = function (n) {
    slides[currentSlide].classList.remove('show')
    currentSlide = (n + slides.length) % slides.length
    slides[currentSlide].classList.add('show')
    // Update the slide count in the slideshow footer
    setSlideCount()
    // Update the slide count in the page form
    slideNumber.value = currentSlide
  }

  /**
   * Advance the slideshow one slide forward
   */
  var nextSlide = function () {
    // Do nothing on the last slide
    if (currentSlide === slides.length - 1) return
    return goToSlide(currentSlide + 1)
  }

  /**
   * Go one slide backward in the slideshow
   */
  var previousSlide = function () {
    // Do nothing on the first slide
    if (currentSlide === 0) return
    return goToSlide(currentSlide - 1)
  }

  /**
   * Set the slide count in the footer of the page
   * @returns {String} HTML for slide count in the slideshow footer
   */
  var setSlideCount = function () {
    var footer = document.getElementById('main_footer')
    return footer.innerHTML = (currentSlide) + ' of ' + (slides.length - 1)
  }

  /**
   * Remove an element from the document
   */
  var removeFromDOM = function (elem) {
    // Check if the element is in the document
    if(!elem.parentNode) return
    return elem.parentNode.removeChild(elem)
  }

  /**
   * Handle click events
   */
  var clickHandler = function (event) {

    // When the slideshow control button is clicked
    if(event.target.closest('#controls')) {
      return form.classList.toggle('show')
    }

    // When the start button is clicked
    if(event.target.closest('#start')) {
      removeFromDOM(start)
      return goToSlide(0)
    }

    // When the cancel button on the navigation form is clicked
    if(event.target.closest('[data-cancel]')) {
      // Hide the form
      return form.classList.toggle('show')
    }

    // Reject all other clicks outside the slideshow
    // Also keeps the slideshow from advancing when the form is visible
    if(!event.target.closest('#slideshow')) return

    // Any other valid click advances the slideshow
    return nextSlide()

  }

  /**
   * Handle keyboard events
   */
  var keyboardHandler = function (event) {

    // Keys to go forward in the slideshow
    if (event.keyCode === 13 || // enter
      event.keyCode === 32 || // spacebar
      event.keyCode === 39 || // right arrow
      event.keyCode === 76) { // L
      return nextSlide()
    }

    // Keys to go backward in the slideshow
    if (event.keyCode === 37 || // left arrow
      event.keyCode === 72) { // H
      return previousSlide()
    }

    return

  }

  /**
   * Handle submit events
   */
  var submitHandler = function (event) {

    // Only handle submissions for the actual form
    if(!event.target.matches('#goToSlideNumber')) return

    // Prevent the browser from reloading the page
    event.preventDefault()

    // If the slideshow has not already started, remove the start button
    if(start) removeFromDOM(start)

    // Convert the form input to from a string to a number
    // And show that numbered slide
    goToSlide(Number(slideNumber.value))

    // Hide the form
    return form.classList.toggle('show')
  }

  // Event Listeners
  document.addEventListener('click', clickHandler, false)
  document.addEventListener('keydown', keyboardHandler, false)
  document.addEventListener('submit', submitHandler, false)

})()
