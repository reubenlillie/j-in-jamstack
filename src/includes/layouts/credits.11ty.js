/**
 * @file The nested credits template for images
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/languages/javascript/#optional-data-method 11ty docs}
 */

/**
 * Creates a new Content template
 * @class
 */
class Credits {

  data() {
    return {
      layout: 'layouts/content'
    }
  }

  render(data) {
    return `
      <ul class="no-list-style">
        <header>
          <h2>Images</h2>
        </header>
        ${data.images.map(function (img) {
          return `<li>${img.name}: ${img.credit}</li>`
        }).join('')
        }
      </ul>
    `
  }

}

module.exports = Credits
