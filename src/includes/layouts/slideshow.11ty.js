/**
 * @file The nested slideshow template
 * @author Reuben L. Lillie
 * See {@link https://www.11ty.io/docs/languages/javascript/#optional-data-method 11ty docs}
 */

/**
 * Creates a new Slideshow template
 * @class
 */
class Slideshow {

  data() {
    return {
      layout: 'layouts/base'
    }
  }

  render(data) {
    return `
    <form id="goToSlideNumber">
      <fieldset class="grid justify-content-center text-center padding">
        <label for="slideNumber">Slide Number</label>
        <input id="slideNumber" name="slideNumber" type="number" min="0" max="${data.collections.slides.length - 1}" value="0" required>
        <button type="submit">Go to Slide</button>
        <button type="button" data-cancel>Cancel</button>
      </fieldset>
    </form>
    <main id="slideshow">
      <button id="start" type="button" class="text-block">
        <span class="padding border-radius box-shadow">Start Slideshow</span>
      </button>
      <article id="slides" class="grid margin padding">
        ${this.slideshow(data)}
        <footer id="main_footer" class="small gray"></footer>
      </article>
    </main>
    <button id="controls">
      <img alt="" src="/includes/assets/images/jam-jars.svg">
    </button>
    <script>
      ${this.minifyJS(this.fileToString('includes/assets/js/text-blocks.js'))}
    </script>
    <script>
      ${this.minifyJS(this.fileToString('includes/assets/js/slideshow.js'))}
    </script>
    <script>
      ${this.minifyJS(this.fileToString('/includes/assets/js/image-links.js'))}
    </script>
    `
  }

}

module.exports = Slideshow
